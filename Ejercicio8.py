# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-

import random


# Procedimiento para imprimir las matrices
def imprimir(matriz, n):
    a = ''
    for i in range(len(matriz)):
        for j in range(len(matriz)):
            a += str(matriz[i][j]) + ''
        a += "\n"
    print("Matriz", n, ":")
    print(a)


# Procedimiento para generar la matriz invertida
def nueva(matriz):
    matriz.reverse()
    imprimir(matriz, 2)


# Procedimiento para generar una matriz aleatoria
def matriz():
    matriz = []
    for i in range(4):
        matriz.append([])
        for j in range(4):
            matriz[i].append(random.randint(0, 9))
    imprimir(matriz, 1)
    nueva(matriz)


# Función principal o main
if __name__ == "__main__":
    matriz()
