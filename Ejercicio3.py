# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


# Procedimiento para imprimir los triangulos
def imprimir(a, altura):
    triangulo = ''
    for i in range(altura):
        for j in range(altura):
            triangulo += a[i][j]
        triangulo += '\n'
    print(triangulo)


# Procedimiento para generar el segundo triangulo
def triangulo2(a, altura):
    a = []
    for i in range(altura):
        a.append([])
        for j in range(altura):
            if j == (altura-1)-i:
                a[i].append('\\')
            if i == j:
                if i >= (2 * i + 1):
                    a[i].append('/ ')
            if i == (altura-1):
                a[i].append('_')
            else:
                a[i].append(' ')
    a.reverse()
    imprimir(a, altura)


# Procedimiento para generar el primer triangulo
def triangulo1(altura):
    a = []
    for i in range(altura):
        a.append([])
        for j in range(altura):
            if j == (altura-1)-i:
                a[i].append('/')
            if i == j:
                if i >= ((altura * 2) - 1):
                    a[i].append('\\ ')
            if i == (altura-1):
                a[i].append('_')
            else:
                a[i].append(' ')
    imprimir(a, altura)
    triangulo2(a, altura)


# Función principal o main
if __name__ == "__main__":
    # Evalua que el usuario ingrese un numero
    try:
        altura = int(input('Ingresa la altura del trangulo: '))
        triangulo1(altura)

    except ValueError:
        print('Eso no es un número.')
