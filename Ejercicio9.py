# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


# Procedimineto para imprimir la matriz de identidad
def imprimir(matriz):
    a = ''
    for i in range(6):
        for j in range(6):
            a += str(matriz[i][j]) + ' '
        a += '\n'
    print(a)


# Procedimiento para generar la matriz de identidad
def matriz_identidad():
    matriz = [[(1 if i == j else 0) for j in range(6)]
              for i in range(6)]
    return imprimir(matriz)


# Función principal o main
if __name__ == "__main__":
    matriz_identidad()
