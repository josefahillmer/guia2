# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


# Procedimiento para ordenar de manor a mayor la lista
def ordenar(entrada):
    for i in range(len(entrada)):
        for j in range(i):
            if entrada[i] < entrada[j]:
                aux = entrada[i]
                entrada[i] = entrada[j]
                entrada[j] = aux
    print("Salida: ", entrada)


# Función principal o main
if __name__ == "__main__":
    entrada = [7, 6, 3, 9, 1, 2, 5, 8, 4]
    print("Entrada: ", entrada)
    ordenar(entrada)
