# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


# Procedimiento para invertir la lista original
def invertida(lista):

    inversion = []
    for i in range(len(lista)):
        inversion.append(lista[i-1])
        i -= 1
    return inversion


# Función principal o main
if __name__ == "__main__":

    lista = ["Di", "buen", "día", "a", "papá"]
    nueva = invertida(lista)
    print("Lista original: ", lista)
    print("Lista invertida: ", nueva)
