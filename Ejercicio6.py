# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


# Procedimiento para evaluar las repeticiones
def eliminar(frase):
    # Cuenta cuantas veces se repite la palabra
    a = frase.count("Agua")
    b = frase.count("Animales")
    c = frase.count("Vida")
    d = frase.count("Muerte")

# Evalua cual es el elemento que más se repite
    if a > b and a > c and a > d:
        print("Elemento que más se repite: ", "Agua")
    if b > a and b > c and b > d:
        print("Elemento que más se repite: ", "Animales")
    if c > a and c > b and c > d:
        print("Elemento que más se repite: ", "Vida")
    if d > b and d > a and d > c:
        print("Elemento que más se repite: ", "Muerte")

# Evalua el elemento que menos se repite
    if a < b and a < c and a < d:
        print("Elemento que menos se repite: ", "Agua")
    if b < a and b < c and b < d:
        print("Elemento que menos se repite: ", "Animales")
    if c < a and c < b and c < d:
        print("Elemento que menos se repite: ", "Vida")
    if d < b and d < a and d < c:
        print("Elemento que menos se repite: ", "Muerte")


# Función principal o main
if __name__ == "__main__":
    frase = ["Agua", "Animales", "Agua", "Vida", "Muerte", "Animales",
             "Vida", "Agua"]
    print("Frase original: ", frase)
    print("Frase nueva: ", list(set(frase)))
    eliminar(frase)
