# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


# Procedimiento para imprimir las listas sin repticiones
def imprimir(nueva):
    print("Lista original: ", nueva)
    print("Lista nueva: ", list(set(nueva)))


# Función principal o main
if __name__ == "__main__":
    a = [1, 2, 3]
    b = [1, 5, 6]
    c = [5, 6, 7]

    # Concatenación de listas
    nueva = a + b + c
    imprimir(nueva)
