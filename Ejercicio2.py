# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


# Procedimiento para imprimir los triangulos
def imprimir(matriz):
    a = ""
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            a += matriz[i][j] + ' '
        a += '\n'
    print(a)


# Procedimiento para generar los triangulos
def triangulo(altura):
    matriz = []
    for i in range(altura):
        matriz.append([])
        for j in range(altura*2):
            if j > i and j < altura*2-i-1:
                matriz[i].append(" ")
            else:
                matriz[i].append("*")
    imprimir(matriz)


# Función principal o main
if __name__ == "__main__":
    # Evalua que el usuario ingrese un numero
    try:
        altura = int(input('Ingrese la altura del triangulo: '))
        triangulo(altura)

    except ValueError:
        print('No ingreso un numero')
