# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-

import random


# Procedimiento para imprimir lo requerido utiliando listas por compresion
def listas (numeros, n):
    mayores = [i for i in range (len(numeros)) if i > n]
    menores = [i for i in range (len(numeros)) if i < n]
    iguales = [i for i in range (len(numeros)) if i == n]
    multiplos = [i for i in range (len(numeros)) if i % n == 0]
    print("Lista original: ", numeros)
    print("Entero: ", n)
    print("Los numeros menores son: ", menores)
    print("Los numeros mayores son: ", mayores)
    print("Los numeros iguales son: ", iguales)
    print("Los multiplos de son: ", multiplos)


# Función principal o main
if __name__ == "__main__":

    numeros = [n for n in range (1, 31)]
    n = random.randrange(1, 31)
    listas (numeros, n)
