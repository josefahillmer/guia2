# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


# Procedimiento para evaluar cuales son las palabras que solo
# Se encuntran en la segunda lista
def unica2(lista1, lista2):
    unica2 = []
    for i in lista2:
        if i not in lista1:
            unica2 += [i]
    print("Palabras que aparecen solo en la segunda lista: ", unica2)


# Procedimiento para evaluar cuales son las palabras que solo
# Se encuntran en la primera lista
def unica1(lista1, lista2):
    unica1 = []
    for i in lista1:
        if i not in lista2:
            unica1 += [i]
    print("Palabras que aparecen solo en la primera lista: ", unica1)


# Procedimiento que evalua las palabras repetidas
def ambas(lista1, lista2, lista):
    ambas = []
    for i in (lista):
        contador1 = lista1.count(i)
        contador2 = lista2.count(i)
        if contador1 > 0 and contador2 > 0:
            # Evalua si la palabra esta en ambas listas
            ambas.append(i)
        ambas2 = list(set(ambas))
    print("Palabras que aparecen en las dos listas: ", ambas2)


# Función principal o main
if __name__ == "__main__":
    lista1 = ["Jose", "Maria", "Mario", "Catalina", "Juan"]
    lista2 = ["Javiera", "Mario", "Tomas", "Catalina", "Juan"]
    lista = lista1 + lista2
    print("Primera lista: ", lista1)
    print("Segunda lista: ", lista2)
    ambas(lista1, lista2, lista)
    unica1(lista1, lista2)
    unica2(lista1, lista2)
