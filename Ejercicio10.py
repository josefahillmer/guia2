# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


# Procedimiento para imprimir el mensaje
def imprimir(mensaje):
    a = ''
    for i in range(len(mensaje)):
        a += str(mensaje[i]) + ' '
    print("Mensaje desifrado: ", a)


# Procedimeinto para sacar las x
def sacar(mensaje):
    eliminar = mensaje.count("X")
    for i in range(eliminar):
        mensaje.remove("X")
    imprimir(mensaje)


# Procedimiento para dar vuelta el mensaje
def dar_vuelta(m):
    mensaje = []
    for i in reversed(m):
        mensaje.append(i)
    sacar(mensaje)


# Función principal o main
if __name__ == "__main__":
    m = "XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt"
    print("Mensaje original: ", m)
    dar_vuelta(m)
